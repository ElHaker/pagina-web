from django.shortcuts import render

from WebApp.models import Datos


def bienvenido(request):
     mensajes ={
           "msg1":"valor de las variables"
     }

     no_datos= Datos.objects.count()
     datos=Datos.objects.all()
     return render(request, "Pagina1/Hola.html", {'no_datos':no_datos,'datos':datos})

def mostrar_datos(request,id):
     dato= Datos.objects.get(pk=id)
     return render(request,'Pagina1/Detalle.html',{'dato':dato})
