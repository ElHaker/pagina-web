from django.db import models

# Create your models here.
from django.utils import timezone


class Datos(models.Model):

    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    fecha_nac = models.DateTimeField(default=timezone.now)

    def publicar(self):

        self.fecha_nac = timezone.now()
        self.save()

    def __str__(self):
        return self.nombre
